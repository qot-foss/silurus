(ns clojure-site-bb
  (:require [babashka.deps :as deps]))

;; (require '[silurus.debug :as dbg])
;; (require '[silurus.debug.hickory :as hdbg])

(deps/add-deps '{:deps {fr.grunwald/silurus {:local/root ".."}}})

(require '[silurus.core :as sil])
(require '[silurus.parsers.hickory :as shs])
(require '[silurus.interceptors :as sitcp])
(require '[silurus.utils :as utils])


(def seed [{:url "https://clojure.github.io/clojure/"
            :analyzer ::process-root}])

(def config {:max-actions 100
             :action-chain ["silurus.interceptors/timer"
                            (sitcp/make-cache-requests-interceptor "/tmp/silurus-cache")]
             :store {:path "/tmp/clojure"
                     :engine :silurus.store.files}
             :output "/tmp/clojure-data"})

(defn process-root [env act-node]
  (let [dom (act-node :parsed)
        current-url (act-node :final-url)
        ns-links (shs/select (shs/id "api-link") dom)
        ns-actions (map (fn [n] (let [href (some->> n :attrs :href (sil/merge-urls current-url))]
                                  {:url href
                                   :analyzer ::process-ns}))
                        ns-links)]
    [env act-node ns-actions]))

(defn process-var [var]
  {:name (shs/select-first-text (shs/tag :h2) var)
   :type (shs/select-first-text (shs/id "var-type") var)
   :usage (shs/select-first-text (shs/id "var-usage") var)
   :docstr (shs/select-first-text (shs/id "var-docstr") var)})

(defn process-ns [env act-node]
  (let [dom (act-node :parsed)
        current-url (act-node :final-url)
        long-name (shs/select-first-text (shs/id "long-name") dom)
        ns-docstr (some->> dom (shs/select-first (shs/id "namespace-docstr")) (shs/extract-formated-text))
        vars-node (shs/select-first (shs/id "var-section") dom)
        entry-nodes (shs/select (shs/id "var-entry") vars-node)
        vars (map process-var entry-nodes)
        ns-data {:data {:name long-name
                        :docstr ns-docstr
                        :vars vars}
                 :ident long-name
                 :entity :namespace}]
    [env act-node [ns-data]]))

(defn scrap []
  (utils/<?? (sil/start-engine-worker! config seed)))

(defn -main [] (scrap))

(when *command-line-args*
  (-main))
