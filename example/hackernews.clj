(ns hackernews
  (:require [silurus.utils :as utils]
            [silurus.parsers.hickory :as shs]
            [hickory.select :as hs]
            [clojure.edn :as edn]
            [silurus.core :as sil]
            [silurus.interceptors :as sitcp]))

(def seed [{:url "https://news.ycombinator.com/"
            :analyzer :hackernews/process-articles-list}])

(def config {:max-actions 5
             :action-chain ["silurus.interceptors/timer"
                            (sitcp/make-cache-requests-interceptor "/tmp/silurus-cache")]})

(defn process-articles-list [env act-node]
  (let [dom (act-node :parsed)
        current-url (act-node :final-url)
        items-list (shs/select-first (hs/child (hs/class "itemlist")
                                               (hs/tag :tbody))
                                     dom)
        item-nodes (partition 3 (filter map? (:content items-list)))
        data-nodes (for [[rank1 rank2 :as n] item-nodes
                         :let [title (shs/select-first-text (hs/class "titlelink") rank1)]
                         :when title
                         :let [link (->> (shs/select-first (hs/tag :a) rank1)
                                         :attrs
                                         :href
                                         (utils/merge-urls current-url))
                               score (some->> (shs/select-first-text (hs/class "score") rank2)
                                              (re-find #"^\d+")
                                              (edn/read-string))
                               comments (some->> (shs/select-first-text (hs/find-in-text #"comments?$") rank2)
                                                 (re-find #"^\d+")
                                                 (edn/read-string))]]
                     {:data {:url link :title title :score score :comments comments} :entity :link})
        next (shs/select-first (hs/class "morelink") dom)
        next-link (-> next :attrs :href)









        next-nodes (if next-link
                     (list {:url (utils/merge-urls current-url next-link)
                            :analyzer :hackernews/process-articles-list
                            :node :act})
                     '())]
    [env act-node (concat data-nodes next-nodes)]))

(defn scrap []
  (sil/start-engine-worker! config seed))
