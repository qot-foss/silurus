(ns hackernews-bb
  (:require [clojure.edn :as edn]
            [babashka.deps :as deps]
            [clojure.core.async :as as]))

(deps/add-deps '{:deps {fr.grunwald/silurus {:local/root ".."}}})
(deps/add-deps '{:deps {fr.grunwald/silurus-store-datalevin {:local/root "../../silurus-store-datalevin"}}})

(require '[babashka.pods :as pods])
(pods/load-pod 'retrogradeorbit/bootleg "0.1.9")

(require '[pod.retrogradeorbit.hickory.select :as hs])
(require '[silurus.core :as sil])
(require '[silurus.utils :as utils])
(require '[silurus.parsers.hickory :as shs])

(def seed [{:url "https://news.ycombinator.com/"
            :analyzer ::process-articles-list}])

(def config {:max-actions 5
             :output "/tmp/hn"
             :store {:engine :silurus.store.datalevin
                     :uri "/tmp/hn-db"}})

(defn process-articles-list [env act-node]
  (let [dom (act-node :parsed)
        current-url (act-node :final-url)
        items-list (shs/select-first (hs/child (hs/class "itemlist")
                                               (hs/tag :tbody))
                                     dom)
        item-nodes (partition 3 (filter map? (:content items-list)))
        data-nodes (for [[rank1 rank2 :as n] item-nodes
                         :let [title (shs/select-first-text (hs/class "titlelink") rank1)]
                         :when title
                         :let [link (->> (shs/select-first (hs/tag :a) rank1)
                                         :attrs
                                         :href
                                         (utils/merge-urls current-url))
                               score (some->> (shs/select-first-text (hs/class "score") rank2)
                                              (re-find #"^\d+")
                                              (edn/read-string))
                               comments nil ;; (some->> (shs/select-first-text (hs/find-in-text #"comments?$") rank2)
                                            ;;      (re-find #"^\d+")
                                            ;;      (edn/read-string))
                               ]]
                     {:data {:url link :title title :score score :comments comments} :entity :link})
        next (shs/select-first (hs/class "morelink") dom)
        next-link (-> next :attrs :href)
        next-nodes (if next-link
                     (list {:url (utils/merge-urls current-url next-link)
                            :analyzer :hackernews/process-articles-list
                            :node :act})
                     '())]
    [env act-node (concat data-nodes next-nodes)]))

(defn scrap []
  (sil/start-engine-worker! config seed))

(as/<!! (scrap))

;; (Thread/sleep 50000)
