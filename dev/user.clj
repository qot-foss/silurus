(ns user
  (:require [taoensso.timbre :as log]
            [silurus.core :as sil]
            [hickory.select :as hs]
            [clojure.string :as str]))

(log/set-level! :debug)
