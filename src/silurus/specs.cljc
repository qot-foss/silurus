(ns silurus.specs
  (:require [medley.core :as med]))

(defn specific-resolve [action f]
  (let [act-sym (symbol action)
        n (namespace act-sym)]
    (require [(symbol (name n))])
    (resolve (symbol (name n) (name f)))))

(defn make-ident [env node]
  (if-let [local-make-ident (:make-ident env)]
    (local-make-ident env node)
    (let [specific-ident (specific-resolve (:action node) 'make-ident)]
      (specific-ident env node))))

(defn coerce-act-node
  [env node]
  (let [starting-node (if (map? node) node {::node node})
        n (-> starting-node
              (update :node #(or % :act))
              (update :action #(or % #?(:bb  :silurus.fetchers.http-kit/request
                                        :clj :silurus.fetchers.hato/request)))
              (update :status #(or % :pending)))
        specific-coerce (specific-resolve (:action n) 'coerce-act-node)
        full-n (if specific-coerce (specific-coerce env n) n)]
    (if (:ident full-n)
      full-n
      (let [ident (make-ident env full-n)]
        (assoc full-n :ident ident)))))

(defn coerce-data-node
  [env node]
  (assoc (merge {:export true
                 :entity :silurus.core/default-entity}
                node)
         :node :data))

(defn node-type [n]
  (cond
    (:node n) (:node n)
    (:action n) :act
    (:data n) :data
    (:url n) :act
    (:entity n) :data
    (:analyzer n) :act
    :else :act))

(comment
  {:node :act ;; optional
   :url "http://example.com"
   :action :silurus.fetchers.hato/fetch ;; optional here
   :ident "http://example.com" ;; optional
   :analyzer 'my/analyzer
   :extra-data {} ;; optional
   :parent "http://root.fr" ;; optional
   :data-parent "example" ;; optional
   })

(comment
  {:node :data
   :entity :brand
   :source "https://example.com"
   :parent "example"
   :data {:my "data"}})
