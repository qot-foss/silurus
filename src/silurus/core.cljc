(ns silurus.core
  (:require [cheshire.core :as json]
            [silurus.store :as sto]
            [silurus.utils :as utils]
            [taoensso.timbre :as log]
            [medley.core :as med]
            [exoscale.interceptor :as itcp]
            [clojure.core.async :as as]
            #?@(:bb [[babashka.deps :as deps]
                     [babashka.fs :as fs]
                     [silurus.parsers.hickory :as hick]]
                :clj
                [[silurus.parsers.hickory :as hick]
                 [babashka.fs :as fs]])
            [clojure.string :as str]))

(log/set-level! :info)

(defn checkout-next-action! [env]
  (sto/checkout-next-action! (:store env) env))

(defn commit-nodes! [env exprs]
  (sto/commit-nodes! (:store env) env exprs))

(defn node-by-ident [env node-type ident]
  (sto/node-by-ident (:store env) env node-type ident))

(defn node-exists? [env node-type ident]
  (sto/node-exists? (:store env) env node-type ident))

(defn data-nodes-list [env & [entity]]
  (sto/data-nodes-list (:store env) env entity))

(defn error-nodes-list [env]
  (sto/error-nodes-list (:store env) env))

(defn retry-action-nodes! [env idents]
  (sto/retry-action-nodes! (:store env) env idents))

(defn parent [env ident]
  (sto/parent (:store env) env ident))

(defn data-children [env ident]
  (sto/data-children (:store env) env ident))

(defn act-children [env ident]
  (sto/act-children (:store env) env ident))

(defn data-ancestors [env ident]
  (sto/data-ancestors (:store env) env ident))

(defn act-ancestors [env ident]
  (sto/act-ancestors (:store env) env ident))

(defn start-context
  ([config seed]
   (utils/go-try
    (let [store (sto/make-store config)
          starting-ctx {:config (dissoc config :components) :store store
                        :actions-count 0 :runs [{:start (System/currentTimeMillis)}]
                        :components (:components config {})}
          seed-normalizer (fn [n]
                            (->> n
                                 (sto/normalize-act-node starting-ctx nil)
                                 (vector :create)))
          init-store (if seed
                       (utils/<? (sto/commit-nodes! store starting-ctx (map seed-normalizer seed)))
                       store)]
      (assoc starting-ctx :store init-store))))
  ([config] (start-context config nil)))

(defn dump-scrap-results!
  ([env path entities]
   (let [filepath (fs/path path "output.edn")
         filedir (fs/parent filepath)
         data-nodes (if entities
                      (mapcat (fn [entity] (data-nodes-list env entity)) entities)
                      (data-nodes-list env))]
       (fs/create-dirs filedir)
       (fs/delete-if-exists filepath)
       (doseq [node data-nodes
               :when (:export node)]
         (spit (str filepath) (str (pr-str node) "\n") :append true))
       (let [error-file (fs/path path "errors.edn")]
         (fs/delete-if-exists error-file)
         (doseq [node (error-nodes-list env)]
           (spit (str error-file) (str (pr-str node) "\n") :append true))))
   env)
  ([env path] (dump-scrap-results! env path nil)))

(defn finalize-crawl [env]
  (log/infof "Finished crawl after %d actions" (:actions-count env))
  (doseq [{:keys [stop]} (:components env)
          :when stop]
    (stop))
  (when-let [path (get-in env [:config :output])]
    (dump-scrap-results! env path))
  env)

(defn resolve-chain [c]
  (mapv (fn [it] (if (map? it) it (utils/resolve-var it))) c))

(defn clean-context-for-print [ctx]
  (-> ctx
      (med/dissoc-in [:store] [:components] [:act-node :parsed]
                     [:config :parser-chain] [:config :action-chain] [:config :analyzer-chain])
      (med/update-existing-in [:act-node :parent] :ident)
      (med/update-existing-in [:act-node :result :body] count)))

(defn clean-exception-for-serializing [e]
  (if (instance? Exception e)
    {:message (ex-message e)
     :data (let [data (dissoc (ex-data e) :context)]
             (update-vals data #(if (fn? %) (str %) %)))
     :cause (clean-exception-for-serializing (ex-cause e))}
    e))

(defn process-action [{:keys [act-node config] :as ctx}]
  (try
    (let [act (-> act-node (:action) (utils/resolve-interceptor))
          act-chain (resolve-chain (:action-chain config []))
          exec-ctx (-> ctx
                       (assoc :phase :action)
                       (assoc-in [:act-node :execution-log :action :name] (:name act)))
          new-ctx (itcp/execute exec-ctx (conj act-chain act))]
      (assoc-in new-ctx [:act-node :execution-log :action :ok] true))
    (catch Exception e
      (throw (ex-info "Error with action of node" {:context (assoc (clean-context-for-print ctx)
                                                                   :phase :action)
                                                   :phase :action}
                      e)))))

(defn mime-based-parser-selector [_ action]
  (let [headers (-> action :result :headers)
        content-type (headers "content-type")
        parser     (cond
      (re-find #"^text/html" content-type) (utils/make-interceptor hick/parse
                                                                   :silurus.parsers.hickory/parse)
      (re-find #"^application/json" content-type) (utils/make-interceptor
                                                   (fn [env {:keys [result] :as action}]
                                                     (let [body (:body result)]
                                                       (if-not (str/blank? body)
                                                         [env (assoc action :parsed (json/decode body true))]
                                                         [env action])))
                                                   :cheshire.core/decode)
      (re-find #"^text/plain" content-type) (utils/make-interceptor identity :identity))]
    (log/tracef "MIME based parser selector: [%s] => %s" content-type (:name parser))
    parser))

(defn process-parsing [{:keys [act-node config] :as ctx}]
  (try
    (let [explicit-parser (or (act-node :parser)
                              (ctx :parser))
          parser-selector (or (act-node :parser-selector)
                              (ctx :parser-selector)
                              mime-based-parser-selector)
          parser (or explicit-parser (parser-selector ctx act-node))
          parser-chain (resolve-chain (:parser-chain config []))
          exec-ctx (-> ctx
                       (assoc :phase :parsing)
                       (assoc-in [:act-node :execution-log :parsing :name] (:name parser)))
          new-ctx (itcp/execute exec-ctx (conj parser-chain parser))]
      (assoc-in new-ctx [:act-node :execution-log :parsing :ok] true))
    (catch Exception e
      (throw (ex-info "Error parsing node" {:context (assoc (clean-context-for-print ctx)
                                                            :phase :parsing)
                                            :phase :parsing}
                      e)))))

(defn process-analysis [{:keys [act-node config] :as ctx}]
  (try
    (let [analyzer (utils/resolve-interceptor (or (act-node :analyzer)
                                                  (-> ctx :config :default-analyzer)))
          analyzer-chain (resolve-chain (:analyzer-chain config []))
          exec-ctx (-> ctx
                       (assoc :phase :analysis)
                       (assoc-in [:act-node :execution-log :analysis :name] (:name analyzer)))
          new-ctx (itcp/execute exec-ctx (conj analyzer-chain analyzer))]
      (assoc-in new-ctx [:act-node :execution-log :analysis :ok] true))
    (catch Exception e
      (throw (ex-info "Error analyzing node" {:context (assoc (clean-context-for-print ctx)
                                                              :phase :analysis)
                                              :phase :analysis}
                      e)))))

(defn run-pipeline [ctx]
  (log/info "Working on node" (:act-node ctx))
  (let [act-result (process-action ctx)
        parse-result (process-parsing act-result)]
    (process-analysis parse-result)))

(defn start-engine-worker! [config seed]
  (log/info "Starting Silurus worker")
  (#?@(:clj [try])
   (let [starting-ctx (utils/<? (start-context config seed))
         {:keys [init]} config
         init-ctx (if init ((utils/resolve-fn init) starting-ctx) starting-ctx)]
     (loop [ctx init-ctx
            pending-act-node (checkout-next-action! init-ctx)]
       (println "PENDING" pending-act-node)
       (if (and pending-act-node (or (not (:max-actions config))
                                     (< (:actions-count ctx) (:max-actions config))))
         (let [[res-ctx res-act-node] (try
                                        (let [{:keys [act-node nodes action-delay]
                                               :or {action-delay (:action-interval config 2000)}
                                               :as new-ctx} (run-pipeline (assoc ctx :act-node pending-act-node))
                                              res-trx (sto/prepare-run-results new-ctx act-node nodes)
                                              new-store (utils/<? (commit-nodes! new-ctx res-trx))]
                                          (log/debug "Finished run pipeline for node" (utils/sumarize-act-node act-node))
                                          (as/<! (as/timeout action-delay))
                                          (let [final-env (-> new-ctx
                                                              (assoc :store new-store)
                                                              (update :actions-count inc)
                                                              (dissoc :act-node :nodes)
                                                              (assoc :action-delay (:actions-interval config 2000)))]
                                            [final-env
                                             (checkout-next-action! final-env)]))
                                        (catch Exception e
                                          (let [{:keys [phase node]} (ex-data e)
                                                true-node (or node pending-act-node)
                                                true-env (:env (ex-data e) ctx)
                                                error-strategy (or (-> true-env :config :error-strategy) :stop)]
                                            (log/error e
                                                       "Error in processing action" (utils/sumarize-act-node true-node)
                                                       "at phase" phase)

                                            (let [new-store (utils/<?
                                                             (commit-nodes! true-env
                                                                            [[:update
                                                                              (assoc true-node
                                                                                     :exception (clean-exception-for-serializing e)
                                                                                     :status :failure)]]))]
                                              (case error-strategy
                                                :stop (do (finalize-crawl true-env)
                                                          (throw e))
                                                :skip (let [new-env (-> true-env
                                                                        (assoc :store new-store)
                                                                        (update :actions-count inc)
                                                                        (dissoc :act-node :nodes)
                                                                        (assoc :action-delay (:actions-interval config 2000)))]
                                                        [new-env
                                                         (checkout-next-action! new-env)])
                                                :restart (let [current-epoch (utils/get-epoch)
                                                               last-restart (last (sort (:restarts true-env [])))]
                                                           (log/debug "Checking for permission to restart.")
                                                           (if (or (nil? last-restart) (> (- current-epoch last-restart) 300))
                                                             (do
                                                               (log/info "Restarting engine.")
                                                               (let [fixed-store (utils/<?
                                                                                  (commit-nodes! (assoc true-env :store new-store)
                                                                                                 [[:update (assoc true-node :status :pending)]]))
                                                                     final-env (-> true-env
                                                                                   (assoc :store fixed-store)
                                                                                   (update :components #(into {} (map (fn [[k {:keys [restart] :as cp}]]
                                                                                                                        (log/debug (str "BEFORE => " cp " -> " restart " -> " (:init config)))
                                                                                                                        (let [after (if restart (restart) cp)]
                                                                                                                          (log/debug (str "AFTER => " after))
                                                                                                                          [k after])) %)))
                                                                                   (update :restarts conj current-epoch))
                                                                     restarted (if (:init config) (do (log/info "Running init.") ((utils/resolve-fn (:init config)) final-env)) final-env)]
                                                              [restarted
                                                               (checkout-next-action! restarted)]))
                                                             (do
                                                               (log/error "Too many retries, shutting down.")
                                                               (finalize-crawl true-env)
                                                               (throw e)))))))))]
           (recur res-ctx res-act-node))
         (finalize-crawl ctx))))))

;; Import from other namespaces for convenience
(utils/import-fns "silurus.utils"
                  ["merge-urls"])

(utils/import-fns "silurus.store"
                  ["parent"
                   "act-children"
                   "data-children"
                   "data-ancestors"
                   "act-ancestors"])
