(ns silurus.store.memory
  (:require [silurus.store :as st]
            [silurus.specs :as specs]
            [medley.core :as med]
            [taoensso.timbre :as log]))

(defn read-node [store env node-type ident]
  (get @(node-type store) ident))

(defn write-node [store env {:keys [ident node] :as n}]
  (let [at (store node)]
    (swap! at assoc ident n)))

(defn nodes-list [store env node-type]
  (vals (deref (get store node-type))))

(defn node-by-ident [store env node-type ident]
  (read-node store env node-type ident))

(defn checkout-next-action! [store env]
  (first (sort-by :score > (filter #(= :pending (:status %)) (nodes-list store env :act)))))

(defn node-exists? [store env node-type ident]
  (if (node-by-ident store env node-type ident) true false))

(defn data-nodes-list
  ([store env entity]
   (let [f (if entity
             #(= entity (:entity %))
             (constantly true))]
     (filter f (nodes-list store env :data))))
  ([store env]
   (data-nodes-list store env nil)))

(defn error-nodes-list [store env]
  (filter #(= :failure (:status %)) (nodes-list store env :act)))

(defn commit-nodes! [store env exprs]
  (let [all-nodes (for [[mode {:keys [ident] :as n}] exprs
                        :let [node-type (specs/node-type n)
                              old-node (node-by-ident store env node-type ident)
                              node (case mode
                                     :create (if old-node nil n)
                                     :update (med/deep-merge old-node n)
                                     :replace n)]
                        :when node]
                    (assoc node :node node-type))
        node-types (group-by :node all-nodes)]
    (log/infof "Adding %d action nodes and %d data nodes." (count (:act node-types)) (count (:data node-types)))
    (doseq [node all-nodes]
      (write-node store env node))
    store))

(defn retry-action-nodes! [store env idents]
  (reduce
   (fn [acc ident]
     (if-let [{:keys [status ident]} (read-node store env :act ident)]
       (if (= status :failure)
         (commit-nodes! store env [:update {:ident ident :node :act :status :pending}])
         acc)
       acc)) store idents))

(defn parent [store env {:keys [parent-ident]}]
  (node-by-ident store env :act parent-ident))

(defn act-children [store env {:keys [act-children-idents]}]
  (map (fn [ident] (node-by-ident store env :act ident)) act-children-idents))

(defn data-children [store env {:keys [data-children-idents]}]
  (map (fn [ident] (node-by-ident store env :data ident)) data-children-idents))

(defn data-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (concat (data-children store env par)
            (lazy-seq (data-ancestors store env par)))))

(defn act-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (conj par (lazy-seq (act-ancestors store env par)))))

(defn make-store [config]
  (with-meta {:act (atom {}) :data (atom {})}
    {`st/node-by-ident (fn [conn env node-type ident] (node-by-ident conn env node-type ident))
     `st/node-exists? (fn [conn env node-type ident] (node-exists? conn env node-type ident))
     `st/checkout-next-action! (fn [this env] (checkout-next-action! this env))
     `st/commit-nodes! (fn [conn env exprs] (commit-nodes! conn env exprs))
     `st/retry-action-nodes! (fn [conn env idents] (retry-action-nodes! conn env idents))
     `st/data-nodes-list (fn [this env entity] (data-nodes-list this env entity))
     `st/error-nodes-list (fn [this env] (error-nodes-list this env))
     `st/parent (fn [this env entity] (parent this env entity))
     `st/act-children (fn [this env entity] (act-children this env entity))
     `st/data-children (fn [this env entity] (data-children this env entity))
     `st/data-ancestors (fn [this env entity] (data-ancestors this env entity))
     `st/act-ancestors (fn [this env entity] (act-ancestors this env entity))}))
