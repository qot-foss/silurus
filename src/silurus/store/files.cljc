(ns silurus.store.files
  (:require [silurus.store :as st]
            [silurus.specs :as specs]
            [medley.core :as med]
            [taoensso.timbre :as log]
            [babashka.fs :as fs]
            [cognitect.transit :as transit]
            [clojure.java.io :as io]
            [cemerick.url :as url]))

(defn ident->filename [ident]
  (str (url/url-encode ident) ".tjson"))

(defn ident->filepath [node-type ident]
  (str (fs/path (name node-type) (ident->filename ident))))

(defn read-file [f]
  (with-open [is (io/input-stream (str f))]
    (transit/read (transit/reader is :json))))

(defn read-node [{:keys [path]} env node-type ident]
  (let [filepath (fs/path path (ident->filepath node-type ident))]
    (when (fs/readable? filepath)
      (read-file filepath))))

(defn write-node [{:keys [path]} env {:keys [ident node] :as n}]
  (let [filepath (fs/path path (ident->filepath node ident))]
    (with-open [os (io/output-stream (str filepath))]
      (transit/write (transit/writer os :json) n))))

(def mem-index-trx (comp (filter #(and (= (specs/node-type (:node %)) :act)
                                       (= (:status %) :pending)))
                         (map (fn [{:keys [ident score]}] [ident score]))))

(defn nodes-list [{:keys [path] :as store} env node-type]
  (let [files (fs/list-dir (fs/path path (name node-type)) "*.tjson")]
    (for [f files]
      (read-file f))))

(defn node-by-ident [store env node-type ident]
  (when-let [data (read-node store env node-type ident)]
    data))

(defn checkout-next-action! [{:keys [action-queue] :as store} env]
  (when-let [[ident _] (first action-queue)]
    (node-by-ident store env :act ident)))

(defn node-exists? [{:keys [path]} env node-type ident]
   (let [file-path (fs/path path (ident->filepath node-type ident))]
    (fs/readable? file-path)))

(defn data-nodes-list
  ([store env entity]
   (let [nodes (nodes-list store env :data)
         pred #(if entity
                 (= (:entity %) entity)
                 (constantly true))]
     (filter pred nodes)))
  ([store env] (data-nodes-list store env nil)))

(defn error-nodes-list
  [store env]
  (let [nodes (nodes-list store env :act)
        pred #(= (:status %) :failure)]
    (filter pred nodes)))

(defn commit-nodes! [store env exprs]
  (let [all-nodes (for [[mode {:keys [ident] :as n}] exprs
                        :let [node-type (specs/node-type n)
                              old-node (node-by-ident store env node-type ident)
                              node (case mode
                                     :create (if old-node nil n)
                                     :update (med/deep-merge old-node n)
                                     :replace n)]
                        :when node]
                    (assoc node :node node-type))
        node-types (group-by :node all-nodes)]
    (log/infof "Adding %d action nodes and %d data nodes." (count (:act node-types)) (count (:data node-types)))
    (reduce
     (fn [acc {:keys [ident score status node] :as n}]
       (write-node acc env n)
       (if (= :act node)
         (cond
           (and (= status :pending)
                (not (seq (filter #(= (:ident %) ident) (:action-queue acc)))))
           (update acc :action-queue (fn [aq tup] (sort-by second (conj aq tup))) [ident score])

           (#{:failure :success} status) (update acc :action-queue (fn [aq ident] (remove #(= (first %) ident) aq)) ident)

           :else acc)
         acc))
     store all-nodes)))

(defn retry-action-nodes! [store env idents]
  (reduce
   (fn [acc ident]
     (if-let [{:keys [status ident]} (read-node store env :act ident)]
       (if (= status :failure)
         (commit-nodes! store env [:update {:ident ident :node :act :status :pending}])
         acc)
       acc)) store idents))

(defn parent [store env {:keys [parent-ident]}]
  (node-by-ident store env :act parent-ident))

(defn act-children [store env {:keys [act-children-idents]}]
  (map (fn [ident] (node-by-ident store env :act ident)) act-children-idents))

(defn data-children [store env {:keys [data-children-idents]}]
  (map (fn [ident] (node-by-ident store env :data ident)) data-children-idents))

(defn data-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (concat (data-children store env par)
            (lazy-seq (data-ancestors store env par)))))

(defn act-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (conj par (lazy-seq (act-ancestors store env par)))))

(defn make-store [{:keys [store]}]
  (let [path (:path store)
        data-path (fs/path path "data")
        act-path (fs/path path "act")]
    (fs/create-dirs data-path)
    (fs/create-dirs act-path)
    (let [nodes (nodes-list store {} :act)
          queue (sort-by second (sequence mem-index-trx nodes))]
      (with-meta (assoc store :action-queue queue)
        {`st/node-by-ident (fn [conn env node-type ident] (node-by-ident conn env node-type ident))
         `st/node-exists? (fn [conn env node-type ident] (node-exists? conn env node-type ident))
         `st/checkout-next-action! (fn [this env] (checkout-next-action! this env))
         `st/commit-nodes! (fn [conn env exprs] (commit-nodes! conn env exprs))
         `st/retry-action-nodes! (fn [conn env idents] (retry-action-nodes! conn env idents))
         `st/data-nodes-list (fn [this env entity] (data-nodes-list this env entity))
         `st/error-nodes-list (fn [this env] (error-nodes-list this env))
         `st/parent (fn [this env entity] (parent this env entity))
         `st/act-children (fn [this env entity] (act-children this env entity))
         `st/data-children (fn [this env entity] (data-children this env entity))
         `st/data-ancestors (fn [this env entity] (data-ancestors this env entity))
         `st/act-ancestors (fn [this env entity] (act-ancestors this env entity))}))))
