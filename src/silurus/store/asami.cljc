(ns silurus.store.asami
  (:require [silurus.store :as st]
            [asami.core :as d]
            [asami.memory :as mem]
            [medley.core :as med]
            [promesa.core :as p]
            [taoensso.timbre :as log]
            [clojure.string :as str]
            [babashka.fs :as fs]
            [clojure.core.async :as as]
            [silurus.utils :as utils])
  (:import [asami.memory MemoryConnection]
           [asami.durable.store DurableConnection]))

(defn parse-storage [uri]
  (if (re-find #"^asami:local" uri)
    :asami-local
    :asami-mem))

(defn make-store [{:keys [store] :as config}]
  (let [uri (:uri store "asami:mem://silurus-default")
        storage (parse-storage uri)]
    (if (= storage :asami-mem)
      (d/create-database uri)
      (let [str-path (str/replace uri #"^asami:local://" "")]
        (if (fs/exists? str-path)
          (log/info "Reusing database at" str-path)
          (do (log/info "Creating dir for database at" str-path)
              (d/create-database uri)))))
    (d/connect uri)))

(defn checkout-next-action! [conn env]
  (let [results (d/q '[:find ?n ?ident ?depth ?score
                       :where
                       [?n :node :act]
                       [?n :status :pending]
                       [?n :db/ident ?ident]
                       [?n :depth ?depth]
                       (optional [?n :score ?score])]
                     conn)]
    (when (seq results)
      (let [sort-type (or (-> env :config :strategy) :breadth-first)
            sort-fn (case sort-type
                      :breadth-first (fn breadth-first [res] (sort-by #(get % 2) < res))
                      :depth-first (fn depth-first [res] (sort-by #(get % 2) > res))
                      :random shuffle)
            [node ident _ _] (first (sort-fn results))
            entity (d/entity conn node)]
        (-> entity
            (med/assoc-some :ident ident))))))

(defn node-exists? [conn env ident]
  (d/q '[:find ?act .
         :in $ ?ident
         :where [?act :db/ident ?ident]]
       conn ident))

(defn override-node [n]
  (med/map-keys #(if-not (#{:ident :db/ident} %)
                   (keyword (str (name %) "'"))
                   %)
                n))

(defn val-to-link [x]
  (cond
    (map? x) x
    (nil? x) nil
    :else {:db/ident x}))

(defn commit-nodes!
  [conn env parent-act-node nodes & {:keys [update-actions]}]
  (let [all-nodes (for [n nodes
                        :let [node (st/prepare-node-for-commit env parent-act-node n)
                              exists? (when (= :act (:node node))
                                        (node-exists? conn env (:ident node)))
                              full (-> (cond
                                         (and exists? (not update-actions)) (select-keys node [:ident])
                                         update-actions (override-node node)
                                         :else node)
                                       (med/assoc-some :db/ident (:ident node))
                                       (med/update-existing :parent (fn [ident] (val-to-link ident)))
                                       (med/update-existing :data-parent (fn [ident] (val-to-link ident)))
                                       (med/update-existing :source (fn [ident] (val-to-link ident)))
                                       (dissoc :ident))]]
                    full)
        parent-act-node-list (if parent-act-node
                               (list {:status' (:status parent-act-node)
                                      :db/ident (:ident parent-act-node)})
                               '())
        node-types (group-by #(if (:data %) :data :act) all-nodes)]
    (log/infof "Adding %d action nodes and %d data nodes." (count (:act node-types)) (count (:data node-types)))
    (let  [ch (as/promise-chan)]
      (p/let [tx (d/transact conn {:tx-data (concat all-nodes parent-act-node-list)})]
        (utils/go-try (as/>! ch conn)))
      ch)))

(defn retry-action-nodes! [conn env idents]
  (doseq [ident idents
          :let [status (d/q '[:find ?status .
                              :in $ ?ident
                              :where
                              [?n :node :act]
                              [?n :db/ident ?ident]
                              [?n :status ?status]]
                            conn ident)]
          :when (= status :failure)]
    (d/transact conn {:tx-data [{:db/ident ident :status' :pending}]})))

(defn nodes-by-ident [conn env idents]
  (for [ident idents
        :let [ent (d/entity conn ident)
              data-children (->> (d/q '[:find [?child-ident ...]
                                        :in $ ?ident
                                        :where
                                        [?n :db/ident ?child-ident]
                                        [?n :node :data]
                                        [?n :source ?ident]]
                                      conn ident))
              action-children (->> (d/q '[:find [?child-ident...]
                                          :in $ ?ident
                                          :where
                                          [?n :db/ident ?child-ident]
                                          [?n :node :act]
                                          [?n :parent ?ident]]
                                        conn ident))]]
    (assoc ent
           :data-children data-children
           :action-children action-children)))

(defn data-nodes-list
  ([conn env entity]
   (let [ids (d/q (if entity
                    '[:find [?ident ...]
                      :in $ ?entity
                      :where
                      [?n :node :data]
                      [?n :entity ?entity]
                      [?n :db/ident ?ident]]
                    '[:find [?n ...]
                      :in $ ?entity
                      :where
                      [?n :node :data]
                      [?n :db/ident ?ident]])
                  conn (or entity '_))]
     (->> ids
          (map #(assoc (d/entity conn %) :db/ident %)))))
  ([conn env] (data-nodes-list conn env nil)))

(defn error-nodes-list [conn env]
  (let [ids (d/q '[:find [?n ...]
                   :where
                   [?n :status :failure]
                   [?n :node :act]]
                 conn)]
    (->> ids
         (map #(d/entity conn %)))))

(extend-protocol st/PSilurusStore
  MemoryConnection
  (st/nodes-by-ident [conn env idents] (nodes-by-ident conn env idents))
  (st/node-exists? [conn env ident] (node-exists? conn env ident))
  (st/checkout-next-action! [this env] (checkout-next-action! this env))
  (st/commit-nodes! [conn env parent-act-node nodes] (commit-nodes! conn env parent-act-node nodes))
  (st/retry-action-nodes! [conn env idents] (retry-action-nodes! conn env idents))
  (st/data-nodes-list [this env entity] (data-nodes-list this env entity))
  (st/error-nodes-list [this env] (error-nodes-list this env))
  DurableConnection
  (st/nodes-by-ident [conn env idents] (nodes-by-ident conn env idents))
  (st/node-exists? [conn env ident] (node-exists? conn env ident))
  (st/checkout-next-action! [this env] (checkout-next-action! this env))
  (st/commit-nodes! [conn env parent-act-node nodes] (commit-nodes! conn env parent-act-node nodes))
  (st/retry-action-nodes! [conn env idents] (retry-action-nodes! conn env idents))
  (st/data-nodes-list [this env entity] (data-nodes-list this env entity))
  (st/error-nodes-list [this env] (error-nodes-list this env)))
