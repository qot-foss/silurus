(ns silurus.debug.hickory
  (:require [silurus.debug :as dbg]
            [hickory.select :as hs]
            [silurus.parsers.hickory :as h]
            [taoensso.timbre :as log]))

(defn select
  ([sel dom] (hs/select sel dom))
  ([sel] (select sel (dbg/current-parsed-body))))

(defn select-locs
  ([sel dom] (hs/select-locs sel dom))
  ([sel] (select-locs sel (dbg/current-parsed-body))))

(defn select-first-loc
  ([sel dom] (h/select-first-loc sel dom))
  ([sel] (select-first-loc sel (dbg/current-parsed-body))))

(defn select-first
  ([sel dom] (h/select-first sel dom))
  ([sel] (select-first sel (dbg/current-parsed-body))))

(defn select-text
  ([sel dom] (h/select-text sel dom))
  ([sel] (select-text sel (dbg/current-parsed-body))))

(defn select-first-text
  ([sel dom] (h/select-first-text sel dom))
  ([sel] (select-first-text sel (dbg/current-parsed-body))))
