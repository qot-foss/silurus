(ns silurus.utils
  (:require [cemerick.url :as url]
            [medley.core :as med]
            [clojure.string :as str]
            [clojure.core.async :as as])
  (:import [java.net URL]
           [clojure.core.async.impl.channels ManyToManyChannel]
           [java.time Instant]))

(defn get-epoch []
  (-> (Instant/now) (.getEpochSecond)))

(defn merge-urls
  "Fills the missing parts of new-url (which can be either absolute,
  root-relative, or relative) with corresponding parts from url
  (an absolute URL) to produce a new absolute URL. Lifted from skyscraper."
  [url new-url]
  (if (str/starts-with? new-url "?")
    (str (str/replace url #"\?.*" "") new-url)
    (str (URL. (URL. url) new-url))))

(defn resolve-var [s]
  (let [symb (symbol s)
        nspace (namespace symb)]
    (require (symbol nspace))
    (deref (resolve symb))))

(defn resolve-fn [s]
  (cond
    (fn? s) s
    :else (resolve-var s)))

(defn make-interceptor [f & [nam]]
  (-> {:enter (fn [ctx]
                (let [act-node (:act-node ctx)
                      env (dissoc ctx :act-node)
                      [new-env new-act-node nodes] (f env act-node)]
                  (-> new-env
                      (assoc :act-node new-act-node)
                      (med/assoc-some :nodes nodes))))}
      (med/assoc-some :name nam)))

(defn resolve-interceptor [s]
  (cond
    (map? s) s
    (or (symbol? s)
        (string? s)
        (keyword? s)) (assoc (make-interceptor (resolve-var s)) :name (keyword (namespace s) (name s)))
    (fn? s) (make-interceptor s)
    (var? s) (assoc (make-interceptor (deref s)) :name (-> s (meta) (:name)))
    :else (throw (ex-info (str "Could not resolve processor " (type s) " " s)
                          {:processor s}))))

(defn sumarize-act-node [n]
  (-> n
      (med/update-existing-in [:result :body] (constantly "..."))
      (med/update-existing-in [:parsed] (constantly '()))
      (med/update-existing-in [:result :headers] (constantly {}))))


(defn throw-err [e]
  (if (instance? Throwable e)
    (throw e)
    e))

(defn chan? [c]
  (instance? ManyToManyChannel c))

(defmacro <? [ch]
  `(let [res# ~ch]
     (if (chan? res#)
       (throw-err (as/<! res#))
       res#)))

(defmacro <?? [ch]
  `(let [res# ~ch]
     (if (chan? res#)
       (throw-err (as/<!! res#))
       res#)))

(defmacro go-try [& body]
  `(as/go
     (try
       ~@body
       (catch Exception e#
         e#))))

;; Lifted from Potemkin
(defmacro import-fn
  "Given a function in another namespace, defines a function with the
   same name in the current namespace.  Argument lists, doc-strings,
   and original line-numbers are preserved."
  ([sym]
     `(import-fn ~sym nil))
  ([sym name]
     (let [vr (resolve sym)
           m (meta vr)
           n (or name (:name m))
           arglists (:arglists m)
           protocol (:protocol m)]
       (when-not vr
         (throw (IllegalArgumentException. (str "Don't recognize " sym))))
       (when (:macro m)
         (throw (IllegalArgumentException.
                  (str "Calling import-fn on a macro: " sym))))
       `(do
          (def ~(with-meta n {:protocol protocol}) (deref ~vr))
          (alter-meta! (var ~n) merge (dissoc (meta ~vr) :name))
          ~vr))))

(defmacro import-fns
  [ns syms]
  (let [all-syms (map #(symbol (name ns) (name %)) syms)]
    `(do ~@(for [sy all-syms]
             `(import-fn ~sy)))))
