(ns silurus.store
  (:require [medley.core :as med]
            [silurus.specs :as sp]
            [silurus.utils :as utils]
            [taoensso.timbre :as log]))

(defprotocol PSilurusStore
  :extend-via-metadata true
  (checkout-next-action! [this env])
  (commit-nodes! [this env exprs])
  (retry-action-nodes! [this env idents])
  (node-by-ident [this env node-type ident])
  (node-exists? [this env node-type ident])
  (data-nodes-list [this env entity] [this env])
  (error-nodes-list [this env])
  (parent [this env entity])
  (act-children [this env entity])
  (data-children [this env entity])
  (data-ancestors [this env entity])
  (act-ancestors [this env entity]))

(defn make-store [config]
  (let [engine (get-in config [:store :engine] :silurus.store.files)
        fname (str (name engine) "/make-store")
        f (utils/resolve-var fname)]
    (log/info "Initialising store with engine" engine)
    (f config)))

(defn assoc-if-absent [m k v]
  (if (= :nil (m k :nil))
    (med/assoc-some m k v)
    m))

(defn normalize-data-node [env parent-act-node n]
  (-> (sp/coerce-data-node env n)
      (assoc-if-absent :parent-ident (:ident parent-act-node))))

(defn normalize-act-node [env parent-act-node n]
  (let [node (sp/coerce-act-node env n)
        depth (or (:depth n)
                 (if-let [depth (:depth parent-act-node)]
                   (inc depth)
                   0))
        strat (or (-> env :config :strategy) :breadth-first)
        score (cond
                (:score n) (:score n)
                (= strat :breadth-first) (if (and depth (not= depth 0))
                                           (/ Float/MAX_VALUE depth)
                                           0)
                (= strat :depth-first) (or depth 0)
                (= strat :random) (rand-int Integer/MAX_VALUE)
                :else 0)]
    (-> node
        (med/assoc-some :parent-ident (:ident parent-act-node))
        (med/assoc-some :depth depth)
        (med/assoc-some :score score))))

(defn prepare-run-results [env current-act-node nodes]
  (let [new-nodes (for [n nodes
                        :let [node-type (sp/node-type n)
                              node (if (= node-type :act)
                                     (normalize-act-node env current-act-node n)
                                     (normalize-data-node env current-act-node n))]]
                    node)
        {:keys [act data]} (group-by :node new-nodes)
        updated-act-node (-> current-act-node
                             (assoc :status :success
                                    :act-children-idents (map :ident act)
                                    :data-children-idents (map :ident data))
                             (dissoc :result :parsed))]
    (conj (map (fn [n] [:create n]) new-nodes)
          [:update updated-act-node])))
