(ns silurus.interceptors
  (:require [clojure.string :as str]
            [clojure.edn :as edn]
            [exoscale.interceptor :as itcp]
            [taoensso.timbre :as log]
            #?@(:clj [[babashka.fs :as fs]])))

(def timer
  {:name :timer
   :enter (fn enter-timer [{:keys [phase] :as ctx}]
            (let [ts (System/currentTimeMillis)]
              (assoc-in ctx [:act-node :execution-log phase :start] ts)))
   :leave (fn leave-timer [{:keys [phase] :as ctx}]
            (if (get-in ctx [:act-node :from-action-cache?])
              ctx
              (let [start (get-in ctx [:act-node :execution-log phase :start])
                    end (System/currentTimeMillis)
                    duration (- end start)]
                (-> ctx
                    (assoc-in [:act-node :execution-log phase :end] end)
                    (assoc-in [:act-node :execution-log phase :duration] duration)))))})

(defn clean-ident-for-disk [ident]
  (str/replace ident #"[:/\\]+" "_"))

(defn make-cache-requests-interceptor
  [path]
  (fs/create-dirs path)
  {:name :cache-requests-interceptor
   :enter
   (fn cache-requests-enter [{:keys [phase act-node] :as ctx}]
     (if (= phase :action)
       (let [{:keys [ident url]} act-node
             filename (clean-ident-for-disk ident)
             full-path (str (fs/path path (str filename ".edn")))]
         (if (and url (fs/readable? full-path))
           (let [cached-node (edn/read-string (slurp full-path))
                 new-node (-> cached-node
                              (merge act-node)
                              (assoc :from-action-cache? true))]
             (log/debugf "Reading action results of %s from disk cache" ident)
             (itcp/terminate (assoc ctx :act-node new-node :action-delay 0)))
           ctx))
       ctx))
   :leave
   (fn cache-requests-leave [{:keys [phase act-node] :as ctx}]
     (if (= phase :action)
       (let [{:keys [ident]} act-node
             filename (clean-ident-for-disk ident)
             full-path (str (fs/path path (str filename ".edn")))]
         (when-not (fs/exists? full-path)
           (spit full-path (pr-str act-node)))
         ctx)
       ctx))})
