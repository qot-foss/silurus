(ns silurus.debug
  (:require [silurus.core :as sil]
            [silurus.specs :as specs]
            [silurus.utils :as utils]
            [taoensso.timbre :as log]
            [exoscale.interceptor :as itcp]))

(defonce current-parsed-body-at (atom nil))
(defonce current-env-at (atom nil))
(defonce current-node-at (atom nil))

(defn current-parsed-body []
  @current-parsed-body-at)

(defn current-env []
  @current-env-at)

(defn current-node []
  @current-node-at)


(defonce last-error-parsed-body-at (atom nil))
(defonce last-error-env-at (atom nil))
(defonce last-error-node-at (atom nil))

(defn last-error-parsed-body []
  @last-error-parsed-body-at)

(defn last-error-env []
  @last-error-env-at)

(defn last-error-node []
  @last-error-node-at)


(defn dummy-analyzer [env node]
  (when-let [parsed (:parsed node)]
    (reset! current-parsed-body-at parsed))
  (reset! current-env-at env)
  (reset! current-node-at node)
  [env node []])

(defn record-action!
  ([env seed]
   (let [actions (if (sequential? seed) seed [seed])
         nodes (map (fn [action] (if (map? action)
                                   (assoc action :analyzer ::dummy-analyzer)
                                   {:url action :analyzer ::dummy-analyzer}))
                    actions)]
     (utils/<?? (sil/start-engine-worker! (assoc env :max-actions 1 :store {:engine :silurus.store.memory}) nodes))))
  ([seed] (record-action! {} seed)))

(defn test-analyzer
  ([analyzer]
   (let [{:keys [ident] :as n} (current-node)
         env (current-env)]
     (log/infof "DEBUG TOOLING -- Reprocessing node ident [%s]" ident)
     (let [[_ _ results] (analyzer env n)
           {:keys [act data]} (group-by specs/node-type results)]
       (log/infof "DEBUG TOOLING -- Found %d data-nodes and %d action-nodes for url [%s]"
                  (count data) (count act) ident)
       results))))

(defn setup-analyzer!
  ([env analyzer]
   (let [ns (namespace analyzer)
         nm (name analyzer)
         {::keys [sample]} (meta (ns-resolve (symbol ns) (symbol nm)))
         act-node (cond
                    (string? sample) {:url sample}
                    (map? sample) sample
                    :else {:action sample})]
     (record-action! env act-node)))
  ([analyzer]
   (setup-analyzer! {} analyzer)))

(defn node-by-ident
  ([env node-type ident]
   (sil/node-by-ident env node-type ident))
  ([node-type ident]
   (node-by-ident (current-env) node-type ident)))

(def debug-analyzer-interceptor
  {:name ::debug-analyzer-interceptor
   :error (fn [{:keys [act-node parsed] :as ctx} err]
            (reset! last-error-parsed-body-at parsed)
            (reset! last-error-env-at (dissoc ctx :act-node))
            (reset! last-error-node-at act-node)
            (itcp/error ctx err))})

(defn retry-last-analysis-error []
  (let [analyzer (utils/resolve-var (:analyzer (last-error-node)))]
    (analyzer (last-error-env) (last-error-node))))
