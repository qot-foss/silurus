(ns silurus.parsers.hickory
  (:refer-clojure :exclude [and or not class])
  (:require [silurus.utils :as utils]
            [clojure.string :as str]))

#?(:bb (do (require '[babashka.pods :as pods])
           (pods/load-pod 'retrogradeorbit/bootleg "0.1.9")
           (require '[pod.retrogradeorbit.hickory.select :as hs])
           (require '[pod.retrogradeorbit.bootleg.html :as html])
           (require '[pod.retrogradeorbit.bootleg.utils :as hutils]))
   :clj (do (require '[hickory.core :as html])
            (require '[hickory.select :as hs])))

(defn parse
  [env {:keys [result] :as action}]
  (let [body (str/trim (:body result))]
    (if-not (str/blank? body)
      [env (assoc action :parsed #?(:bb (hutils/convert-to body :hickory)
                                    :clj (-> body (html/parse) (html/as-hickory))))]
      [env action])))

(defn select-text [sel tree]
  (let [res (hs/select sel tree)]
    (if (seq res)
      (->> res
           (map (fn [{:keys [content]}] (str/join " " (filter string? content)))))
      res)))

(defn select-first-text [sel tree]
  (first (select-text sel tree)))

(defn select-first [sel tree]
  (first (hs/select sel tree)))

(defn select-first-loc [sel dom]
  (first (hs/select-locs sel dom)))

(defn extract-formated-text [root]
  (let [segs (loop [todo (if (seq? root) root [root])
                    txt []]
               (if-let [elt (first todo)]
                 (cond
                   (string? elt) (recur (rest todo) (conj txt elt))
                   (= :lb elt) (if (= :lb (peek txt))
                                 (recur (rest todo) txt)
                                 (recur (rest todo) (conj txt :lb)))
                   :else
                   (let [{:keys [tag content]} elt]
                     (cond
                       (= :br tag) (recur (rest todo) (conj txt "\n\n"))
                       (#{:div :p :h1 :h2 :h3 :h4 :h5 :tr :li} tag) (recur (concat [:lb] content [:lb] (rest todo)) txt)
                       (#{:td} tag) (recur (concat ["\t"] content ["\t"] (rest todo)) txt)
                       :else (recur (concat content (rest todo)) txt))))
                 txt))]
    (str/trim (str/join "" (map #(if  (= % :lb) "\n\n" %) segs)))))

;; Alias most fns from hickory.select for convenience
(utils/import-fns #?(:bb "pod.retrogradeorbit.hickory.select"
                     :clj "hickory.select")
                  ["tag"
                   "node-type"
                   "attr"
                   "id"
                   "class"
                   "any"
                   "element"
                   "root"
                   "count-until"
                   "n-moves-until"
                   "until"
                   "left-pred"
                   ;; "find-in-text"
                   "element-child"
                   "nth-of-type"
                   "right-pred"
                   "nth-last-of-type"
                   "left-of-node-type"
                   "nth-child"
                   "right-of-node-type"
                   "nth-last-child"
                   "first-child"
                   "last-child"
                   "and"
                   "or"
                   "not"
                   "el-not"
                   "child"
                   "follow-adjacent"
                   "precede-adjacent"
                   "descendant"
                   "follow"
                   "precede"
                   "select-next-loc"
                   "after-subtree"
                   "has-descendant"
                   "has-child"
                   "select-next-loc"
                   "select-locs"
                   "select"])

#?(:bb (defn find-in-text [sel dom] (throw (ex-info "Not implemented in babashka" {})))
   :clj (utils/import-fns "hickory.select"
                          ["find-in-text"]))
