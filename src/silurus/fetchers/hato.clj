(ns silurus.fetchers.hato
  (:require [hato.client :as http]
            [medley.core :as med]))

(def default-client-options {:redirect-policy :always
                             :connect-timeout 10000})

(defn start [config]
  (let [start-client-fn #(http/build-http-client (merge default-client-options (::http-client config)))
        client (start-client-fn)]
    {:component client :stop (fn [] nil) :restart #(start config)}))

(defn coerce-act-node
  [_ node]
  (if-let [url (:silurus.specs/node node)]
    (-> node
        (assoc :url url)
        (dissoc :silurus.specs/node))
    node))

(defn make-ident [env node]
  (:url node))

(defn request [{:keys [components config] :as env}
               {:keys [url options] :as action}]
  (let [options-with-default (if (= ::not-found (:timemout options ::not-found))
                               (assoc options :timeout 5000)
                               options)
        {:keys [component] :as cp} (get components ::http-client (start config))
        res (http/request  (-> (assoc options-with-default :url url)
                               (med/assoc-some :http-client component)))]
    [(assoc-in env [:components ::http-client] cp)
     (assoc action
            :result (med/dissoc-in res
                                   [:request :http-client]
                                   [:http-request]
                                   [:http-client]
                                   [:request :http-request])
            :final-url (-> res :request :url))]))
