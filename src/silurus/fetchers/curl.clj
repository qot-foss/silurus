(ns silurus.fetchers.curl
  (:require [babashka.curl :as http]))

(def default-client-options {:method :get})

(defn start [config]
  (let [opts (merge default-client-options (::http-client config))]
    {:component opts :stop (fn [] nil) :restart #(start config)}))

(defn coerce-act-node
  [_ node]
  (if-let [url (:silurus.specs/node node)]
    (-> node
        (assoc :url url)
        (dissoc :silurus.specs/node))
    node))

(defn make-ident [env node]
  (:url node))

(defn request [{:keys [components config] :as env}
               {:keys [url options] :as action}]
  (let [{:keys [component] :as cp} (get components ::http-client (start config))
        res (http/request (merge options component {:url url}))]
    [(assoc-in env [:components ::http-client] cp)
     (assoc action
            :result (dissoc res :exit :process)
            :final-url (-> res :request :url))]))
