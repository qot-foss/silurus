(ns silurus.fetchers.http-kit
  (:require [org.httpkit.client :as http]
            [medley.core :as med]
            [clojure.walk :as walk]))

(def default-client-options {:connect-timeout 10000
                             :idle-timeout 30000})

(defn start [config]
  {:component (http/make-client (merge default-client-options (::http-client config)))
   :stop (fn [] nil)
   :restart #(start config)})

(defn coerce-act-node
  [_ node]
  (if-let [url (:silurus.specs/node node)]
    (-> node
        (assoc :url url)
        (dissoc :silurus.specs/node))
    node))

(defn make-ident [env node]
  (:url node))

(defn request [{:keys [components config] :as env}
               {:keys [url options] :as action}]
  (let [{:keys [component] :as cp} (get components ::http-client (start config))
        res (deref (http/request (-> (assoc options :url url)
                                     (med/assoc-some :client component))))]
    [(assoc-in env [:components ::http-client] cp)
     (assoc action
            :result (-> res
                        (dissoc :opts)
                        (update :headers walk/stringify-keys))
            :final-url url)]))
